// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic','starter.controllers','ngMaterial','ngCordova','ionic-datepicker','ionic-toast','ionic-timepicker','pdf','chart.js'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})




.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider


   .state('achpage1', {
              url: '/achpage1',
              templateUrl: 'templates/achpage1.html'
          })

             .state('achpage2', {
                      url: '/achpage2',
                      templateUrl: 'templates/achpage2.html'
                  })


                   .state('achpage3', {
                              url: '/achpage3',
                              templateUrl: 'templates/achpage3.html'
                          })

                           .state('achpage4', {
                                      url: '/achpage4',
                                      templateUrl: 'templates/achpage4.html'
                                  })

                                   .state('achpage5', {
                                              url: '/achpage5',
                                              templateUrl: 'templates/achpage5.html'
                                          })



.state('map', {
    url: '/',
    templateUrl: 'templates/map.html',
    controller: 'MapCtrl'
  })

  .state('chart', {
      url: '/chart',
      templateUrl: 'templates/chart.html',
      controller: 'chartCtrl'
    })

.state('actpage1', {
              url: '/actpage1',
              templateUrl: 'templates/actpage1.html'
          })

             .state('actpage2', {
                      url: '/actpage2',
                      templateUrl: 'templates/actpage2.html'
                  })


                   .state('actpage3', {
                              url: '/actpage3',
                              templateUrl: 'templates/actpage3.html'
                          })

                           .state('actpage4', {
                                      url: '/actpage4',
                                      templateUrl: 'templates/actpage4.html'
                                  })

                                   .state('actpage5', {
                                              url: '/actpage5',
                                              templateUrl: 'templates/actpage5.html'
                                          })
                                    .state('candidate', {
                                                  url: '/candidate',
                                                  templateUrl: 'templates/candidate.html'
                                              })
                                     .state('reachus', {
                                                   url: '/reachus',
                                                   templateUrl: 'templates/reachus.html'
                                               })


.state('evepage1', {
              url: '/evepage1',
              templateUrl: 'templates/evepage1.html'
          })

             .state('evepage2', {
                      url: '/evepage2',
                      templateUrl: 'templates/evepage2.html'
                  })


                   .state('evepage3', {
                              url: '/evepage3',
                              templateUrl: 'templates/evepage3.html'
                          })

                           .state('evepage4', {
                                      url: '/evepage4',
                                      templateUrl: 'templates/evepage4.html'
                                  })

                                   .state('evepage5', {
                                              url: '/evepage5',
                                              templateUrl: 'templates/evepage5.html'
                                          })
     .state('home', {
                   url: '/home',
                   templateUrl: 'templates/home.html'
               })

.state('education', {
              url: '/education',
              templateUrl: 'templates/education.html'
          })

.state('survival', {
              url: '/survival',
              templateUrl: 'templates/survival.html'
          })

.state('pdfdownload', {
              url: '/pdfdownload',
              templateUrl: 'templates/pdf.html',
                  controller: 'DocumentController'

          })

.state('previous', {
              url: '/previous',
              templateUrl: 'templates/previous.html'
          })

.state('slideview', {
                          url: '/slideview',
                          templateUrl: 'templates/slideview.html',
                              controller: 'slideCtrl'

                      })

.state('modalwindow', {
                          url: '/modalwindow',
                          templateUrl: 'templates/modalwindow.html',
                              controller: 'modalwindowCtrl'

                      })
 .state('popup', {
               url: '/popup',
               templateUrl: 'templates/popup.html',
                   controller: 'PopupCtrl'

           })


    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })


  .state('login', {
        url: '/login',
        templateUrl: 'templates/login.html',
         controller: 'AppCtrl'
    })

    .state('election', {
            url: '/election',
            templateUrl: 'templates/election.html'
        })
     .state('datepicker', {
                 url: '/datepicker',
                 templateUrl: 'templates/datepicker.html',
                 controller: 'datepickCtrl'

             })

      .state('timepicker', {
                       url: '/timepicker',
                       templateUrl: 'templates/timepicker.html',
                       controller: 'timepickCtrl'

                   })
     .state('toaster', {
                      url: '/toaster',
                      templateUrl: 'templates/toaster.html',
                      controller: 'toastCtrl'

                     })


     .state('action', {
                 url: '/action',
                 templateUrl: 'templates/action.html'
             })

      .state('information', {
                  url: '/information',
                  templateUrl: 'templates/information.html'
              })

        .state('products2', {
                    url: '/products2',
                    templateUrl: 'templates/products2.html'
                })
         .state('history', {
                     url: '/history',
                     templateUrl: 'templates/history.html'
                 })

   .state('app.about', {
       url: '/about',
       views: {
         'menuContent': {
           templateUrl: 'templates/about.html',
             controller: 'aboutCtrl'


         }
       }
     })

     .state('products', {
           url: '/products',
       templateUrl: 'templates/products.html'
})

     .state('app.services', {
            url: '/services',
            views: {
              'menuContent': {
                templateUrl: 'templates/services.html',
                controller: 'serviceCtrl'

              }
            }
          })

    .state('app.quality', {
                url: '/quality',
                views: {
                  'menuContent': {
                    templateUrl: 'templates/quality.html',
                     controller: 'qualityCtrl'

                  }
                }
              })

     .state('app.page4', {
                 url: '/page4',
                 views: {
                   'menuContent': {
                     templateUrl: 'templates/page4.html'
                   }
                 }
               })

.state('app.page5', {
                 url: '/page5',
                 views: {
                   'menuContent': {
                     templateUrl: 'templates/page5.html'
                   }
                 }
               })

.state('app.page6', {
                 url: '/page6',
                 views: {
                   'menuContent': {
                     templateUrl: 'templates/page6.html'
                   }
                 }
               })

.state('app.page7', {
                 url: '/page7',
                 views: {
                   'menuContent': {
                     templateUrl: 'templates/page7.html'
                   }
                 }
               })

.state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/aboutUs.html'
      }
    }
  })

  .state('app.browse', {
      url: '/browse',
      views: {
        'menuContent': {
          templateUrl: 'templates/contactus.html'
        }
      }
    })
    .state('app.index1', {
      url: '/index1',
      views: {
        'menuContent': {
          templateUrl: 'templates/index1.html',
          controller: 'indexCtrl'
        }
      }
    })

  .state('app.single', {
    url: '/playlists/:playlistId',
    views: {
      'menuContent': {
        templateUrl: 'templates/courseDetails.html',
        controller: 'PlaylistCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/');
});
